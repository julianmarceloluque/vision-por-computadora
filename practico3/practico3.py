#! /usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import cv2

if (len(sys.argv)>1):
    filename = sys.argv[1]
else :
    print( ' Pass afilename as first argument ' )
    sys.exit( 0 )

cap = cv2.VideoCapture(filename)
fourcc = cv2.VideoWriter_fourcc( 'X ' , 'V ' , ' I ' , 'D ' )

#cv2.CAP_PROP_FRAME_WIDTH   # 3
#cv2.CAP_PROP_FRAME_HEIGHT  # 4
#cv2.CAP_PROP_FPS           # 5
#cv2.CAP_PROP_FRAME_COUNT   # 7

width = int(cap.get(3))
height = int(cap.get(4))
fps = int(cap.get(5))
print('ancho, altura, fps: ', width, height, fps)
framesize = (width , height)

out = cv2.VideoWriter('output.avi',fourcc,20.0,framesize)

while(cap.isOpened()) :
    ret,frame = cap.read()
    if ret is True:
        gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
        out.write(gray)
        cv2.imshow('Image gray',gray)
        if cv2.waitKey(fps)&0xFF == ord('q'):
            break
    else :
        break

cap.release()
out.release()
cv2.destroyAllWindows()