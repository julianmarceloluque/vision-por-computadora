import cv2
import numpy as np

drawing = False #verdadero si el click del mouse es presionado

point0 = [[-1, -1], [-1, -1], [-1, -1],[-1,-1]] 
i = 0

def select_4_points(event,x,y,flags,param): #seleccionamos 4 puntos en un orden espcífico para graficar
    global point0, point1, i, flag

    if event == cv2.EVENT_LBUTTONUP:
        if i == 0:
            point0[0] = x,y
            cv2.circle(img_aux,(x,y),3,(0,255,0),-1)
            i = i + 1
        elif i == 1:
            point0[1] = x,y
            cv2.circle(img_aux,(x,y),3,(0,255,0),-1)
            i = i + 1
        elif i == 2:
            point0[2] = x,y
            cv2.circle(img_aux,(x,y),3,(0,255,0),-1)
            i = i + 1
        elif i == 3:
            point0[3] = x,y
            cv2.circle(img_aux,(x,y),3,(0,255,0),-1)
            flag = 1  

def distance_between(p1, p2):
	#Devuelve el escalar de la distancia entre dos puntos
	a = p2[0] - p1[0]
	b = p2[1] - p1[1]
	return np.sqrt((a ** 2) + (b ** 2))

def transformacion_homografica(img_aux):

    top_left, top_right, bottom_right, bottom_left = point0[0], point0[1], point0[2], point0[3] #4 puntos seleccionados en la imagen
    src = np.array([top_left, top_right, bottom_right, bottom_left], dtype='float32') #coloco los 4 puntos en un arreglo llamado 'src'
    rows = int(max(distance_between(bottom_right, top_right),distance_between(top_left, bottom_left))) #valor máxico de filas (entre lado derecho y lado izquierdo)
    columns = int(max(distance_between(top_left, top_right),distance_between(bottom_right, bottom_left))) #valor máxico de columnas (entre lado superior y lado inferior)

    side = int(max([ #calculo un promedio entre todos los lados
		distance_between(bottom_right, top_right),
		distance_between(top_left, bottom_left),
		distance_between(bottom_right, bottom_left),
		distance_between(top_left, top_right)
	]))

    print(side)
    dst = np.array([[0, 0], [columns - 1, 0], [columns - 1, rows - 1], [0, rows - 1]], dtype='float32') #arreglo 'dst' para los 4 puntos 
    map_matrix = cv2.getPerspectiveTransform(src, dst) #matriz entre los puntos de 'src' y 'dst'
    img_perspective = cv2.warpPerspective(img_aux, map_matrix, (int(columns), int(rows))) #obtengo imagen seleccionada por puntos en perspectiva

    return img_perspective 
    

img = cv2.imread('img/pizarron.jpg',1) #leo la imagen .jpg
img_copy = img.copy()           #copio la imagen en una variable copy  
img_aux = img.copy()            #copio la imagen en una variable auxiliar

print('Bienvenido a la transformación de homografía.\n')
print('Elija 4 puntos para ver la perspectiva en el orden:')
print('1) Esquina superior izquierda')
print('2) Esquina superior derecha')
print('3) Esquina inferior derecha')
print('4) Esquina inferior izquierda\n')
print('-Luego, presione la tecla h para realizar la transformada.\n')
print('-Presione "r" para restaurar la imagen\n')
print('-Presione "q" para salir del programa.')
cv2.namedWindow('Pizarron')  #nombre de la ventana

cv2.setMouseCallback('Pizarron',select_4_points) #llamo a la función 'selec_4_points'

while(1): #while infinito
    cv2.imshow('Pizarron',img_aux) #muestro la imagen leida anteriormente
    k = cv2.waitKey(1) & 0xFF
    if k == ord('h'):
        img_transformada = transformacion_homografica(img_aux) #obtengo la imagen en perspectiva a través de una función pasandole la 'img_aux' 
        cv2.imwrite('img/pizarron_transformado.jpg', img_transformada)
        cv2.namedWindow('Pizarron transformado')
        cv2.imshow('Pizarron transformado', img_transformada)
        i = 0 #coloco en 0 la variable 'i' para la selección de puntos
    elif k == ord('r'): #'r' para restaurar la imagen original
        img_aux = img_copy.copy()
        cv2.destroyWindow('Pizarron transformado') #cuando restauro, se cierra la ventana de la imagen con transformación homográfica
    elif k == ord('q'): #'q' para salir del programa
        break

cv2.waitKey(0) 