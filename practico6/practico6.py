import cv2
import numpy as np 
import math

drawing = False #verdadero si el click del mouse es presionado

ix,iy =-1,-1 #coordenadas iniciales inicializadas en -1
fx,fy =-1,-1 #coordenadas finales inicializadas en -1
aux_x, aux_y = -1,-1 #coordenadas auxiliares (finalmente sin utilizar)


def rectangle(event,x,y,flags,param):
    global ix,iy,fx,fy,drawing
    
    if event == cv2.EVENT_LBUTTONDOWN: #evento inicial del mouse (click izquierdo)
        ix, iy = x, y #coordenadas iniciales
        drawing = True #
    elif event == cv2.EVENT_MOUSEMOVE: #evento movimiento del mouse
        if drawing is True:            
            fx, fy = x, y #coordenadas finales
            cv2.rectangle(img_aux,(ix,iy),(fx,fy),(255,0,0),thickness=2)
    elif event == cv2.EVENT_LBUTTONUP: #evento final del mouse (suelto click izquierdo)
        fx, fy = x, y #coordenadas finales
        drawing = False
        cv2.rectangle(img,(ix,iy),(fx,fy),(255,0,0),thickness=2) #rectangulo de cortado azul

def similaridad(img,rows,columns,tx,ty,angle,s):
    (rows,columns) = (img.shape[0],img.shape[1])

    rad_angle = math.radians(angle)

    #M = np.float32([[s*np.cos(rad_angle),s*np.sin(rad_angle),tx],
    #                [s*(-np.sin(rad_angle)),s*np.cos(rad_angle),ty]])
    M = np.float32([[s*np.cos(rad_angle),s*np.sin(rad_angle),tx],
                    [s*(-np.sin(rad_angle)),s*np.cos(rad_angle),ty]])

    shifted = cv2.warpAffine(img,M,(columns,rows))
    return shifted

img = cv2.imread('img/minion.jpg',1) #leo la imagen .jpg
img_copy = img.copy()           #copio la imagen en una variable copy  
img_aux = img.copy()            #copio la imagen en una variable auxiliar
print('Bienvenido a la transformación de similaridad.\n')
print('1) Realice un rectángulo para recortar la imagen.\n')
print('2) Presione la tecla "g" para guardar el recorte.\n')
print('3) Presione la tecla "s" para realizar la transformada.\n')
print('\n--> "r" para restaurar la imagen\n')
print('--> "q" para salir del programa.')
cv2.namedWindow('Minion con brackets')  #nombre de la ventana
cv2.setMouseCallback('Minion con brackets',rectangle) #llamo a la función 'rectangle'


while(1): #while infinito
    cv2.imshow('Minion con brackets',img) #muestro la imagen leida anteriormente
    k = cv2.waitKey(1) & 0xFF
    if k == ord ('g'): #'g' para guardar el recorte
        crop_minion = img[(iy+5):(fy-5),(ix+5):(fx-5)] #evito rectangulo de color en la imagen cortada
        cv2.imwrite('img/minion_recortado.jpg',crop_minion)
        cv2.imshow('Minion recortado',crop_minion) #muestro la imagen cortada (aparte de la original)
        flag = 1
    elif (k == ord ('s')):
        if flag == 1:
            (rows,columns) = (crop_minion.shape[0],crop_minion.shape[1])
            print('\nIngrese ángulo de rotación: ')
            angle = int(input())
            print('Ingrese traslación en el eje x: ')
            tx = int(input())
            print('Ingrese traslación en el eje y: ')
            ty = int(input())
            print('Ingrese el factor de escala isotrópico: ')
            s = float(input())
            minion_similarizado = similaridad(crop_minion,rows,columns,tx,ty,angle,s)
            cv2.imwrite('img/minion_similarizado.jpg',minion_similarizado)
            cv2.imshow('Minion recortado',crop_minion)
            cv2.imshow('Minion similarizado', minion_similarizado)
        else:
            print('******Primero recorte la imagen.******')
            continue
    elif k == ord('r'): #'r' para restaurar la imagen original
        img = img_copy.copy()
        cv2.destroyWindow('Minion Similarizado') #cuando restauro, se cierra la ventana de la imagen similarizada
        cv2.destroyWindow('Minion Cortado') #cuando restauro, se cierra la ventana de la imagen cortada
    elif k == ord('q'): #'q' para salir del programa
        break

cv2.destroyAllWindows()