import cv2
import numpy as np



point0 = [[-1, -1], [-1, -1], [-1, -1]] 
i = 0

def select_3_points(event,x,y,flags,param):
    global point0, point1, i

    if event == cv2.EVENT_LBUTTONUP:
        if i == 0:
            point0[0] = x,y
            cv2.circle(img_aux,(x,y),5,(0,255,0),-1)
            i = i+1
        elif i == 1:
            point0[1] = x,y
            cv2.circle(img_aux,(x,y),5,(0,255,0),-1)
            i = i+1
        elif i == 2:
            point0[2] = x,y
            cv2.circle(img_aux,(x,y),5,(0,255,0),-1)
            i = 0


def transformacion_afin(img):
    (rows,columns) = (img.shape[0],img.shape[1])
    origin_points = np.float32([[0,0],[0,rows-1],[columns-1,rows-1]]) #esquina superior izquierda, esquina superior derecha, esquina inferior derecha
    destination_points = np.float32([[point0[0],point0[1],point0[2]]]) #esquina superior derecha, esquina inferior derecha, esquina inferior izquierda
    M = cv2.getAffineTransform(origin_points,destination_points) #matriz entre los puntos origen y destino
    img_transformada = cv2.warpAffine(img,M,(columns,rows))
    return img_transformada


def mascara_minion(minion_transformado,img_copy):
    mascara = minion_transformado.copy() #copio en una máscara el sombrero incrustado en la img_aux de la 'transformacion_afin'
    mascara[mascara != 0] = 255  #los valores en que máscara es distinta de cero (distinta de negro), coloco en 255 dichos valores 
    mascara = cv2.bitwise_not(mascara) #cv2.bitwise_not me lleva los valores de 0 a 255 y los valores de 255 a 0 (operación not) de la máscara
    mascara_final = cv2.bitwise_and(mascara, img_copy) #cv2.bitwise_and : me lleva a 255 sólo los valores que son 255 en máscara y en img_copy,
                                                                        # los demas valores todos en 0.
                                                                        # mascara - img_copy | mascara_final
                                                                        #   0           0    |      0
                                                                        #   0           255  |      0
                                                                        #   255         0    |      0
                                                                        #   255         255  |      255
    return mascara_final


img = cv2.imread('img/minion.jpg',1)
img_banana = cv2.imread('img/banana.jpg',1)

#Dimensiones imagen 1, para redimensionar imagen 2 (para incrustar)
(rows,columns) = (img.shape[0],img.shape[1])
img_banana = cv2.resize(img_banana,(columns,rows))

img_copy = img.copy()           #copio la imagen en una variable copy  
img_aux = img.copy()            #copio la imagen en una variable auxiliar

print('Bienvenido a la transformación afín.\n')
print('Elija 3 puntos para insertar en el orden:\n')
print('Esquina superior derecha\n')
print('Esquina inferior derecha\n')
print('Esquina inferior izquierda\n')
print('-Luego, presione la tecla a para realizar la transformada\n')
print('-"r" para restaurar la imagen\n')
print('-"q" para salir del programa.')
cv2.namedWindow('Minion sin banana')  #nombre de la ventana
cv2.imshow('Minion sin banana', img_aux)
cv2.setMouseCallback('Minion sin banana',select_3_points) #llamo a la función 'selec_3_points'

while(1): #while infinito
    
    cv2.imshow('Minion sin banana',img_aux) #muestro la imagen leida anteriormente
    k = cv2.waitKey(1) & 0xFF
    if k == ord('a'):
        minion_transformado = transformacion_afin(img_banana) #obtengo la imagen transformada a través de una función de transformada afín
        mascara_r = mascara_minion(minion_transformado,img_copy) 
        img_transformada = cv2.bitwise_or(mascara_r, minion_transformado) #cv2.bitwise_or: me lleva todos los valores a 255, menos en los que en ambas imagenes son 0.
        cv2.imwrite('img/minion_con_banana.jpg', img_transformada)
        cv2.namedWindow('Minion con banana')
        cv2.imshow('Minion con banana', img_transformada)

    elif k == ord('r'): #'r' para restaurar la imagen original
        img_aux = img_copy.copy()
        cv2.destroyWindow('Minion con banana') #cuando restauro, se cierra la ventana de la imagen con transformación afín
    elif k == ord('q'): #'q' para salir del programa
        break

cv2.waitKey(0) 
cv2.destroyAllWindows()