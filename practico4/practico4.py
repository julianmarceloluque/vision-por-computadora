import cv2
import numpy as np

drawing = False #True es si el mause esta presionado
mode = True #Si es True dibuja un rectangulo. Presiona m para cambiar la curva
ix, iy = -1, -1

def draw_circle(event, x, y, flags, param):
    global ix, iy, drawing, mode
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix, iy = x, y
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True:
            if mode is True:
                cv2.rectangle(img, (ix, iy), (x, y), (9, 255, 0), -1)
            else:
                cv2.circle(img, (x,y), 5, (0, 0, 255), -1)
    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        if mode is True:
            cv2.rectangle(img, (ix, iy), (x, y), (0, 255, 0), -1)
        else:
            cv2.circle(img, (x, y), 5, (0, 0, 255), -1)

img = np.zeros((512, 512, 3), np.uint8)
cv2.namedWindow('Imagen')
cv2.setMouseCallback('Imagen', draw_circle)
while(1):
    cv2.imshow('Imagen', img)
    k = cv2.waitKey(1) & 0xFF
    if k == ord ('m'):
        mode = not mode
    elif k == ord ('g'):
        cv2.imwrite('img/archivo.png',img)
    elif k == ord ('r'):
        img = np.zeros((512, 512, 3), np.uint8)
    elif k==27:
        break
cv2.destroyAllWindows()