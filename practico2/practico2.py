#! usr/bin/env python
#-*- coding:utf-8-*-

import cv2

img = cv2.imread('img/hoja.png',0)
img_color = cv2.imread('img/hoja.png',1)
cv2.imshow('Ventana2', img)
cv2.imshow('Imagen en color', img_color)

# Metodo 1:
#for i in range(len(img[0])):
#   for j in range(len(img)):
#        if (img[i][j]<200):
#            img[i][j]=0

# Metodo 2:
img[img<200]=0

print('Dimensiones de la Imagen: ', img.shape)
cv2.imshow('Nombre', img)
cv2.imwrite('img/HojaFiltrada.png',img)
cv2.waitKey(0)
